= Concepts

The core conceptual models in the wringer.io system are:

xref:architecture/pipelines.adoc[Pipeline]:: A Pipeline is a reusable sequence of processing stages.
xref:architecture/endpoints.adoc[Endpoint]:: A source or sink of data records.  Usually this will resolve to a Kafka Topic.
Endpoints also hold metadata about the content type and schema of the payload that the Endpoint accepts or produces.
xref:architecture/subscriptions.adoc[Subscription]:: Subscriptions attach a processing Pipeline to a source and sink Endpoint, along with any
configuration necessary to customize the Pipeline and wire up the Endpoints.