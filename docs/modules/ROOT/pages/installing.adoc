= Getting Started

== Getting started with Docker

Wringer is packaged as a set of Docker images.  The easiest way to get started is to run
the Docker image for the UI and give it a whirl.

[source,shell script]
----
docker run
----

Once installed, you can access the UI at \http://localhost:3200 (or whatever port you exposed from the container)
using the username/password admin/admin.

At this point you might want to perform one of these common next steps:

Create and Deploy a Pipeline:: Use the UI to create, deploy, and monitor a pipeline.
Configure the User Interface::
The UI node defaults to a configuration suited for experimental use.  For deployment to more stringent environments, it is common
to configure aspects such as Authentication, SSL, and usually an external database is used.
Connect to a Kubernetes Cluster::
Once you have deployed the UI and can access it through the browser, a common next step is to connect to a Kubernetes
cluster so that worker nodes can be deployed and managed.
