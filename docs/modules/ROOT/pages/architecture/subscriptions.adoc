= Subscriptions

A Subscription ties together a source of records, a processing pipeline, a sink that accepts processed records, and any
configuration required for each of these components.

.Components of a Subscription
image::wringer-subscription.png[]

Subscriptions are long-lived and will generally run continuously when started until stopped through the UI, using
a REST API, or using `kubectl` from the command line.  In this way, a Subscription can be thought of as a processing job.

== Subscription Lifecycle

.Subscription Lifecycle
image::wringer-subscription-lifecycle.png[]