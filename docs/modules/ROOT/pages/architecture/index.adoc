= Wringer Architecture

The Wringer platform is comprised of a set of Java processes based on http://osgi.org[OSGi] (Open Services Gateway Initiative).
OSGi promotes a component-based architecture and development model.  Each OSGi component is packaged as a Java Archive (JAR) file and
runs in an OSGi implementation such as Equinox, Knopflerfish, or Felix.  The Wringer project uses the
https://felix.apache.org/documentation/index.html[Felix] runtime.

== Components
Each Wringer process is deployed as a Docker image.  The images share a core set of components, which are then augmented
with the components that provide the specific application functionality.

=== Common Components
include::{partialsdir}/common-components.adoc[]

=== Admin Container Components
include::{partialsdir}/admin-components.adoc[]

=== Worker Container Components
include::{partialsdir}/worker-components.adoc[]